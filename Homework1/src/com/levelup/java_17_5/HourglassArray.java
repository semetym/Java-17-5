package com.levelup.java_17_5;

import java.util.InputMismatchException;
import java.util.Scanner;
/*
* Вывести на экран и подсчитать сумму чисел в двумерной матрице:
* - только чисел, расположенных слева от диагонали;
* - справа от диагонали;
* - верхнего и нижнего треугольников, образующихся пересечением двух диагоналей;
* - левого и правого треугольника пересекающихся диагоналей.
* (N вводиться с клавиатуры, числа в матрице генерятся)*/
public class HourglassArray {
    public static void main(String[] args) {

        try {
            System.out.print("Enter n:");
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();

            int sum, s1 = 0, s2 = 0, s3 = 0, s4 = 0;
            int [][] arr = new int[n][n];

            System.out.println("Initial View ->");
//initialize array
            for (int i = 0; i < arr.length ; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = (int)(Math.random() * 10); // initialize with numbers 0..9 for better view
                    System.out.print(arr[i][j] + "\t");
                }
                System.out.println();
            }

            System.out.println();

// get sum left diagonal
            System.out.println("Left Diagonal View ->");
            sum = 0;
            for (int i = 0; i < arr.length ; i++) {
                for (int j = 0; j < arr[i].length-i; j++) {
                    System.out.print(arr[i][j] + "\t");
                    sum = sum + arr[i][j];
                }
                System.out.println();
            }
            System.out.println();
            System.out.println("Sum left: = " + sum);
            System.out.println();

// get sum right diagonal
            System.out.println("Right Diagonal View ->");
            sum = 0;
            for (int i = 0; i < arr.length ; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (i + j < arr.length-1)
                    {
                        System.out.print(" " + "\t");
                    }
                    if (i + j == arr.length-1) {
                        System.out.print(arr[i][j] + "\t");
                        sum = sum + arr[i][j];
                    }
                    if (i + j > arr.length-1) {
                        System.out.print(arr[i][j] + "\t");
                        sum = sum + arr[i][j];
                    }
                }
                System.out.println();
            }
            System.out.println();
            System.out.println("Sum right: = " + sum);
            System.out.println();
// Vertical sums hourglass
            System.out.println("Vertical hourglass view ->");
            System.out.println();
            for (int i = 0; i < arr.length ; i++) {
                for (int j = 0; j < arr.length; j++) {

                    if (j>=i && j < (arr.length-i)){
                        s1= s1+arr[i][j];
                        System.out.print(arr[i][j] + "\t");
                    }
                    else if (j<=i && j >= arr.length-i-1){
                        s3= s3+arr[i][j];
                        System.out.print(arr[i][j] + "\t");
                    }
                    else {
                        System.out.print(" " + "\t");
                    }
                }
                System.out.println();
            }
            System.out.println();
            System.out.println("Top triangle sum: =" + s1);
            System.out.println("Bottom triangle sum: =" + s3);
            System.out.println();

// Horizontal sums hourglass
            System.out.println("Horizontal hourglass view ->");
            System.out.println();
            for (int i = 0; i < arr.length ; i++) {
                for (int j = 0; j < arr.length; j++) {

                    if (j>=i && j >= arr.length-i-1){
                        System.out.print(arr[i][j] + "\t");
                        s2= s2+arr[i][j];
                    }
                    else if (j<=i && j < arr.length-i) {
                        System.out.print(arr[i][j] + "\t");
                        s4 = s4 + arr[i][j];
                    }
                    else {
                        System.out.print(" " + "\t");
                    }
                }
                System.out.println();

            }
            System.out.println();

            System.out.println();
            System.out.println("Left Triangle sum: = " + s4);
            System.out.println("Right Triangle sum: = " + s2);
        }

        catch(InputMismatchException e){
            System.err.println("Please enter valid value(digit). " + e.getMessage());
        }
        catch (NegativeArraySizeException e){
            System.err.println("Please enter positive value");
        }
    }


}