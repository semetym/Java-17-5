package com.levelup.java_17_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Вычислить N факториал числа. N вводится с клавиатуры (опционально).
 */

public class Factorial {
    public static void main(String[] args) throws IOException
    {
        try {
            System.out.print("Enter n:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int n_factorial;
            long n_fact_res = 1;

            do {
                String s = reader.readLine();
                n_factorial = Integer.parseInt(s);
                if (n_factorial<0){
                    System.out.println("Factorial is the product of all positive integers, thus it can't be less than 0, please try one more time");
                    System.out.print("Enter n:");
                }
            } while (n_factorial<0);

            if (n_factorial == 0) {
                n_fact_res = 1;
            }
            else {
                for (int i = 1; i <= n_factorial; i++) {
                    n_fact_res = Math.multiplyExact(n_fact_res, i);
                }
            }

            System.out.println("Factorial " + n_factorial + " = "+n_fact_res);
        }
        catch(ArithmeticException e) {
            System.err.println("Overflow for primitive type long, the maximum value exceeded, please try to enter lower value n");
        }
        catch(NumberFormatException e){
                System.err.println("Please enter valid value(digit). " + e.getMessage());
        }

    }

}
