package com.levelup.java_17_5;/*
* 1. Просчитать сумму одно и двухмерного массивов.Массив задать с помощью явной инициализации.
* */

public class ArrayElementary {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6};
        int array[][] = {{0,1,2},{3,4,5},{6,7,8}};
        int sum = 0;

        for (int i = 0; i < arr.length ; i++) {
            System.out.print(arr[i] + "\t");
            sum = sum + arr[i];
        }
        System.out.println();
        System.out.println("Sum of all 1D array elements = " + sum);
        System.out.println();

        sum = 0;
        for (int i = 0; i < array.length ; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
                sum = sum + array[i][j];
            }
            System.out.println();
        }
        System.out.println("Sum of all 2D array elements = " + sum);
    }
}