package com.levelup.java_17_5;
/*
* 2) Создайте класс для работы со стэком. Размер стэка вводится с клавиатуры.
* создайте 3 метода -
 для внесения целого элемента в стэк,
 для извлечения из стэка,
 для просмотра значения элемента в вершины стэка.
* */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EmptyStackException;
import java.util.Stack;

public class StackKeyboardInput {
    public static void main(String[] args) throws IOException {

        System.out.print("Enter N, it will be considered as stack size =>:");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        int stackSize = Integer.parseInt(s);
        System.out.println();


        Stack<Integer> stack = new Stack<>();
        stack = CreateNewStack(stackSize);

        System.out.println("Current stack: " + stack);

        System.out.println("Please choose of listed options:");
        System.out.println("1. Add new element to the stack");
        System.out.println("2. Remove last element from the stack");
        System.out.println("3. Extract top element of the stack");
        System.out.print("=>: ");
        String s2 = reader.readLine();
        int optionChoice = Integer.parseInt(s2);

        if (optionChoice ==  1) {
            AddNew2Stack(stack);
        }
        else if (optionChoice == 2) {
            RemoveFromStack(stack);
        }
        else if (optionChoice == 3) {
            CheckTopFromStack(stack);
        }
        else {
            System.out.println("Any option was chosen :(");
        }
    }

    public static Stack<Integer> CreateNewStack(int n) {

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < n ; i++) {
            stack.push((int)(Math.random() * 10));
        }
    return stack;
    }

    public static void AddNew2Stack(Stack stack) {
        stack.push((int)(Math.random() * 10));
        System.out.println("Current stack after adding new element: " + stack);
    }
    public static void RemoveFromStack(Stack stack) {
        try {
            stack.pop();
        }
        catch (EmptyStackException e) {
            System.err.println("Cannot remove from empty stack");
        }
        System.out.println("Current stack after removing an element: " + stack);
    }
    public static void CheckTopFromStack(Stack stack) {
        System.out.println("Check top element: " + stack.peek().toString());
    }

}
