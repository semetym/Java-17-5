package com.levelup.java_17_5;

/*
1) Подсчитать сумму N>2 && N<6 мерного массива.
Используйте структуру switch-case. Для выбора мерности массива используется меню.*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NDimensionArray {

    public static void main(String[] args) throws IOException
    {

        System.out.print("Enter N dimensions for future sum, possible quantity of dimensions 3,4,5 =>:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        int dimension = Integer.parseInt(s);

        switch (dimension) {
            case 3: {
                SumOfThreeDims();
                break;
            }
            case 4: {
                SumOfFourDims();
                break;
            }
            case 5: {
                SumOfFiveDims();
                break;
            }
        }

    }
    public static void SumOfThreeDims() {

        int threeDim[][][] = new int[3][3][3];
        int sum = 0;
        for (int i = 0; i < threeDim.length ; i++) {
            for (int j = 0; j < threeDim.length; j++) {
                for (int k = 0; k < threeDim.length; k++) {
                    threeDim[i][j][k] = (int)(Math.random() * 10); // initialize with numbers 0..9
                    sum += threeDim[i][j][k];
                }
            }
        }
        System.out.println("Sum of all elements of 3D array:= " + sum);

    }
    public static void SumOfFourDims() {
        int fourDim[][][][] = new int[4][4][4][4];
        int sum = 0;
        for (int i = 0; i < fourDim.length ; i++) {
            for (int j = 0; j < fourDim.length; j++) {
                for (int k = 0; k < fourDim.length; k++) {
                    for (int l = 0; l < fourDim.length ; l++) {
                        fourDim[i][j][k][l] = (int)(Math.random() * 10); // initialize with numbers 0..9
                        sum += fourDim[i][j][k][l];
                    }
                }
            }
        }
        System.out.println("Sum of all elements of 4D array:= " + sum);

    }
    public static void SumOfFiveDims() {
        int fiveDim[][][][][] = new int[5][5][5][5][5];
        int sum = 0;
        for (int i = 0; i < fiveDim.length ; i++) {
            for (int j = 0; j < fiveDim.length; j++) {
                for (int k = 0; k < fiveDim.length; k++) {
                    for (int l = 0; l < fiveDim.length ; l++) {
                        for (int m = 0; m < fiveDim.length; m++) {
                            fiveDim[i][j][k][l][m] = (int)(Math.random() * 10); // initialize with numbers 0..9
                            sum += fiveDim[i][j][k][l][m];
                        }
                    }
                }
            }
        }
        System.out.println("Sum of all elements of 5D array:= " + sum);
    }
}
