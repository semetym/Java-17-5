package com.levelup.java_17_5;

import java.io.IOException;

/*
1) Подсчитать сумму N>2 && N<6 мерного массива.
Используйте структуру switch-case. Для выбора мерности массива используется меню.
2) Создайте класс для работы со стэком. Размер стэка вводится с клавиатуры.
Посмотрите в книге тему Методы класса, и создайте 3 метода - для внесения целого элемента в стэк,
для извлечения из стэка, для просмотра значения элемента в вершины стэка.
Что такое стэк, можно посмотреть на вики.
3) Создайте класс  для работы с очередью.
Создайте 2 метода - для внесения в очередь целочисленного элемента и для извлечения элемента из очереди.
Что такое очередь, можно посмотреть на вики.
* */
public class Main {

    public static <T>void main(String[] args) throws IOException
    {
        /*FIFO implementation using arrays*/

        Queue myQueue = new Queue(5);
        System.out.println("FIFO Queue will have 5 elements size");
        System.out.println("adding elements... ");
        myQueue.addElement2Queue(1);
        myQueue.addElement2Queue(2);
        myQueue.addElement2Queue(3);
        myQueue.addElement2Queue(4);
        myQueue.addElement2Queue(5);
        myQueue.addElement2Queue(111);
        System.out.println("Populated queue: " + (T) myQueue.toString());
        System.out.println("Removing element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println("Queue view after removing: " + (T) myQueue.toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println((T) myQueue.toString());
        System.out.println("Queue view after removing 2 elements: " + (T) myQueue.toString());
        System.out.println("adding new elements..");
        myQueue.addElement2Queue(6);
        myQueue.addElement2Queue(7);
        System.out.println("Newly populated queue: " + (T) myQueue.toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());
        System.out.println("Removing next element from the queue: " + (T) myQueue.removeElementFormQueue().toString());

        System.out.println((T) myQueue.toString());
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();


        /*LIFO queue using two stacks*/
        StackBaseQueue queue = new StackBaseQueue();
        System.out.println("LIFO queue: ");
        queue.addElement2Stack(22);
        queue.addElement2Stack(23);
        queue.addElement2Stack(24);
        queue.addElement2Stack(25);
        System.out.println(queue.toString());
        queue.removeElementFromStack();
        queue.removeElementFromStack();
        queue.removeElementFromStack();

        System.out.println(queue.toString());

    }

}


