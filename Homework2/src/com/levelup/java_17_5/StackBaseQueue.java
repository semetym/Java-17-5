package com.levelup.java_17_5;

import java.util.Stack;

public class StackBaseQueue {
    private Stack<Integer> stack;
    private Stack<Integer> tmpStack;
//StackBase Constructor
    public StackBaseQueue()
    {
        stack = new Stack<Integer>();
        tmpStack = new Stack<Integer>();
    }
//Add an element to the queue
    public void addElement2Stack(int data){
             if (stack.isEmpty()) {
                 stack.push(data);
             }
             else {
                 while (!stack.isEmpty()){
                     tmpStack.push(stack.pop());
                 }
                 stack.push(data);
                 while (!tmpStack.isEmpty()){
                     stack.push(tmpStack.pop());
                 }
             }
        }
//Remove front element from the queue
    public void removeElementFromStack() {
        stack.pop();
    }

    @Override
    public String toString() {
        return "Queue: " + stack.toString();
    }
}


