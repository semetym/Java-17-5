/*Array sort of implementation*/
package com.levelup.java_17_5;

import java.util.Arrays;

public class Queue<T> {
    private int front;
    private int rear;
    private int size;
    private T[] queue;
// Queue constructor
    public Queue(int queueSize) {
        size = queueSize;
        queue = (T[]) new Object[size];
        front = -1;
        rear = -1;
    }
// recreate array added new element
    public void caseArrayQueueFull() {
        size++;
        T[] tempQueue = (T[]) new Object[size];
        System.arraycopy(queue,0,tempQueue,0,queue.length);
        queue=tempQueue;
    }
// check if queue is empty
    public boolean isEmpty() {
        return (front == -1 && rear == -1);
    }
// add new element to queue
    public void addElement2Queue(T value) {
        if ((rear + 1) % size == front) {
            caseArrayQueueFull();
            if (front !=0) {
                front--;
                queue[front] = value;
            }
            else
                rear=(rear + 1) % size;
            queue[rear] = value;

        } else if (isEmpty()) {
            front++;
            rear++;
            queue[rear] = value;

        } else {
            if (front !=0) {
                front--;
                queue[front] = value;
            }
            else {
                rear = (rear + 1) % size;
                queue[rear] = value;
            }
        }
    }
//remove element from queue
    public T removeElementFormQueue() {
        T value = null;
        if (isEmpty()) {
            throw new IllegalStateException("Cannot remove any element when queue is empty");
        } else if (front == rear) {
            value = queue[front];
            front = -1;
            rear = -1;

        } else {
            value = queue[front];
            front = (front + 1) % size;
        }
        return value;

    }

    @Override
    public String toString() {
        return "Queue [Head=" + front + ", Tail=" + rear + ", size=" + size + ", queue=" + Arrays.toString(queue);
    }
}
