package com.levelup.java_17_5;

public class LimitClass {
    private static int objCount = 0;

    public LimitClass(){
        if (objCount >= 3) {
        throw new ArrayIndexOutOfBoundsException();

        }
        objCount++;
    }

}
