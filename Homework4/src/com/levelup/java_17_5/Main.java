package com.levelup.java_17_5;
/*
1. Создать класс, из которого можно создать не более 3 обьектов.
2. Создать класс синглтон, который всегда при попытке создании нового обьекта всегда возвращает ссылку на один и тот же обьект.
3. Создать класс согласно патерну builder, в котором при создании обьекта можно сразу же инициализировать часть характеристик
 нужными нам значениями без использования инициализирующих конструкторов. Используйте внутренний класс.
 */
public class Main {
/*3 objects limit*/
    public static void main(String[] args) {
// Limit3
        System.out.println("/*LIMIT3*/");
        LimitClass obj1 = new LimitClass();
        LimitClass obj2 = new LimitClass();
        LimitClass obj3 = new LimitClass();
//        LimitClass obj4 = new LimitClass();

        System.out.println(obj1);
        System.out.println(obj2);
        System.out.println(obj3);
//        System.out.println(obj4);

/*Lazy singleton implementation*/
        System.out.println();
        System.out.println("/*SINGLETON*/");
        Singleton instance1 = Singleton.getInstance();
        Singleton instance2 = Singleton.getInstance();

        System.out.println(instance1);
        System.out.println(instance2);

/*Builder pattern*/
        System.out.println();
        System.out.println("/*BUILDER*/");

        Foo element = new Foo.Builder().property1(23).property2(123123).property3(54).property4(1).build();
        Foo element2 = new Foo.Builder().property2(766).property4(888).build();
        System.out.println("Property1: " + element.getProperty1() + "\n" + "Property2: " + element.getProperty2() + "\n" + "Property3: " + element.getProperty3() + "\n" + "Property4: " + element.getProperty4());
        System.out.println();
        System.out.println("Property2: " + element2.getProperty2() + "\n" + "Property4: " + element2.getProperty4());


    }
}
