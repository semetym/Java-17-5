package com.levelup.java_17_5;

public class Foo {
    private int property1;
    private int property2;
    private int property3;
    private int property4;

    public int getProperty1() {
        return property1;
    }

    public int getProperty2() {
        return property2;
    }

    public int getProperty3() {
        return property3;
    }

    public int getProperty4() {
        return property4;
    }

public static class Builder {
        private int property1;
        private int property2;
        private int property3;
        private int property4;
        public Builder property1(int value) {
            this.property1 = value;
            return this;
        }
        public Builder property2(int value) {
            this.property2 = value;
            return this;
        }
        public Builder property3(int value) {
            this.property3 = value;
            return this;
        }
        public Builder property4(int value) {
            this.property4 = value;
            return this;
        }
        public Foo build() {
            return new Foo(this);
        }
    }

    private Foo(Builder obj) {
        this.property1 = obj.property1;
        this.property2 = obj.property2;
        this.property3 = obj.property3;
        this.property4 = obj.property4;
    }
}
