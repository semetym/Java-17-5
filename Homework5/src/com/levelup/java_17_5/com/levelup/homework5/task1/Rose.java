package com.levelup.java_17_5.com.levelup.homework5.task1;

import com.levelup.java_17_5.com.levelup.homework5.task1.Flower;

public class Rose extends Flower {
    private int price = 50;

    public Rose(){

    }

    @Override
    public int getPrice() {
        return price;
    }
}
