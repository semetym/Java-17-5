package com.levelup.java_17_5.com.levelup.homework5.task1;

import com.levelup.java_17_5.com.levelup.homework5.task1.Flower;

public class GooseGrass extends Flower {
    private int price = 5;

    public GooseGrass() {
    }

    @Override
    public int getPrice() {
        return price;
    }
}
