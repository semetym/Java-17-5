package com.levelup.java_17_5.com.levelup.homework5.task1;

import com.levelup.java_17_5.com.levelup.homework5.task1.HierFlow;
import com.levelup.java_17_5.com.levelup.homework5.task2.Class1;
import com.levelup.java_17_5.com.levelup.homework5.task2.MonsterClass;

/*
* Дз.
1) Необходимо реализовать иерархию цветов (для примера, пусть это будут розы, гвоздики, тюльпаны и... что-то на свой вкус).
Создать несколько объектов-цветов. Собрать букет с определением его стоимости. В букет может входить несколько цветов одного типа.
2). Унаследовать характеристики и поведение 3-х классов в одном потомке.
3) Eсть класс Mail (письмо) в котором есть 3 характеристики - head, body и footer.
 И метод writeMail -> который печатает письмо, состоящее из этих характеристик.
  Создать механизм, который позволит нам переопределять body письма, например для разных праздников.
  (Хочу чтобы вы расмотрели эту задачу с точки зрения паттерна Декоратор)
* */
public class Main {

    public static void main(String[] args)  {
	// Task1

    HierFlow bouquet = new HierFlow();
    bouquet.createFlower();
    bouquet.collectBouqet();
        bouquet.print();
        System.out.println("Bouquet size: = " + bouquet.size());
        System.out.println("Bouquet Price: = " + bouquet.getBouqetPrice());


    // Task2
        System.out.println();
        System.out.println("Task2");

        MonsterClass.NestedMonsterClass.NestedMonsterClass2.NestedMonsterClass3 obj = new MonsterClass().new NestedMonsterClass().new NestedMonsterClass2().new NestedMonsterClass3();

        obj.printAll();
        obj.print3();



//        MonsterClass.NestedMonsterClass


    }
}
