package com.levelup.java_17_5.com.levelup.homework5.task1;

public class Budiaks extends Flower {

    private int price = 1;

    public Budiaks() {
    }

    @Override
    public int getPrice() {
        return price;
    }
}
