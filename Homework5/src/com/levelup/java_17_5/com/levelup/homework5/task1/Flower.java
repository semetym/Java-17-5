package com.levelup.java_17_5.com.levelup.homework5.task1;

public abstract class Flower {
    private int price;

    public Flower() {
    }

    public abstract int getPrice();

}
