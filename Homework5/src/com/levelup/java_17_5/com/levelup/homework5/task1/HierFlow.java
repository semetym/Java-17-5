package com.levelup.java_17_5.com.levelup.homework5.task1;

import java.util.ArrayList;


public class HierFlow {

    private Flower[] flowers = new Flower[4];
    private ArrayList<Flower> flowerArrayList = new ArrayList<>();
    private final int rand = oddRandom();

    public void createFlower() {
        flowers[0] = new Rose();
        flowers[1] = new Tulip();
        flowers[2] = new GooseGrass();
        flowers[3] = new Budiaks();
    }

    private Flower selectFlower() {
        return flowers[(int) (Math.random() * 4)];
    }

    private int oddRandom(){

        int rand = (int)(Math.random()*20 + 1);
        boolean checkOdd = true;
        do {
            if (rand % 2 == 0) {
                rand = (int)(Math.random()*20 + 1);
            }
            else checkOdd = false;
        }
        while (checkOdd);

    return rand;
    }

    public void collectBouqet() {
        for (int i = 0; i < rand; i++) {
            flowerArrayList.add(selectFlower());
        }
    }

    public int getBouqetPrice(){
        int sum=0;
        for (int i = 0; i < flowerArrayList.size() ; i++) {
            sum+=flowerArrayList.get(i).getPrice();
        }
        return sum;
    }
    public int size() {
       return flowerArrayList.size();
    }

    public void print() {
        for (int i = 0; i < flowerArrayList.size(); i++) {
            System.out.println("Added to bouqet: =>" + flowerArrayList.get(i).getClass());
        }
        System.out.println();
    }

}
