package com.levelup.java_17_5.com.levelup.homework5.task1;

import com.levelup.java_17_5.com.levelup.homework5.task1.Flower;

public class Tulip extends Flower {
    private int price = 10;

    public Tulip() {
      }

    @Override
    public int getPrice() {
        return price;
    }

}
