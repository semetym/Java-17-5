package com.levelup.java_17_5;

public class Main {

    public static void main(String[] args) {
    LimitlessStack stack = new LimitlessStack();
// unlimited Stack
    stack.push(123);
    stack.push(124);
    stack.push(125);

     System.out.println("who is the last?: " + stack.peek());
     System.out.println("get out of here: " + stack.pop());
     System.out.println("who is the last?: " + stack.peek());
        stack.print();

        System.out.println();
        System.out.println("LIFO QUEUE");
        System.out.println();
        // LIFO limitless QUEUE
     LimitlessQueue queue = new LimitlessQueue();
     queue.push(3245);
     queue.push(23);
     queue.push(2);
     queue.push(35);
     queue.push(11);

        System.out.print("QUEUE => :");
        queue.print();
        System.out.println("Pop from QUEUE => :" + queue.pop());
        System.out.print("QUEUE => :");
        queue.print();

    // Prioritized queue
        System.out.println();
        System.out.println("PRIO QUEUE");
        System.out.println();

        PrioritizedLimitlessQueue prQueue = new PrioritizedLimitlessQueue();
        prQueue.push(12,76);
        prQueue.push(1,88);
        prQueue.push(6,123);
        prQueue.push(15,200);
        prQueue.push(4,13);
        System.out.print("QUEUE => :");
        prQueue.print();
        System.out.println(prQueue.pop());
        System.out.print("QUEUE => :");
        prQueue.print();
    }
}
