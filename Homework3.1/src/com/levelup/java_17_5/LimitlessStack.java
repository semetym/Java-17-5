package com.levelup.java_17_5;

public class LimitlessStack<E> extends CustomArrayList {

// base .ctor
    public LimitlessStack()
    {

    }
// push method
    public E push(E element) {
        add(element);
        return  element;
    }
// pop method
    public synchronized E pop() {
        int size = size();
        E element = this.peek();
        remove(size-1);
        return element;

    }
// peek method
    public synchronized E peek() {
        int size = size();
        if (size == 0) {
            throw new IllegalStateException();
        } else {
            return (E) get(size-1);
        }
    }
    //print method
    public void print() {
        System.out.print("[");
        for (int i = 0; i < size() ; i++) {
            if(i == size() - 1) {
                System.out.print(get(i));
            } else {
                System.out.print(get(i) + ", ");
            }
        }
        System.out.print("];");
        System.out.println();
    }
}
