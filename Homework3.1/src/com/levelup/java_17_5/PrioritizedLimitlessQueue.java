package com.levelup.java_17_5;


public class PrioritizedLimitlessQueue<E> {
    // my private nested class
    private class PrioriterizedEntry{
        private int priority;
        private E input;

        //.ctor
        public PrioriterizedEntry(int priority, E input) {
            this.priority = priority;
            this.input = input;
        }
    }
    private LimitlessStack<PrioriterizedEntry> stack;
    //StackBase .ctor
    public PrioritizedLimitlessQueue()
    {
        stack = new LimitlessStack<PrioriterizedEntry>();
    }
    //Add an element to the queue
    public void push(int priority, E value) {
        stack.push(new PrioriterizedEntry(priority, value));
        sort();
    }
    // sort list method
    @SuppressWarnings("unchecked")
    public void sort() {
        LimitlessStack<PrioriterizedEntry> tmpStack = new LimitlessStack<>();

        for (int i = 0; i < stack.size()-1; i++) {
            for (int j = 1; j <stack.size()-i ; j++) {
                if (((PrioriterizedEntry)stack.get(j-1)).priority > ((PrioriterizedEntry)stack.get(j)).priority) {
                    tmpStack.push((PrioriterizedEntry)stack.get(j-1));
                    stack.set(j-1,stack.get(j));
                    stack.set(j,tmpStack.get(0));
                    tmpStack.clear();
                }
            }

        }
    }
    // pop method
    public E pop() {
        sort();
        return stack.pop().input;
    }
    //print method
    public void print() {
        System.out.print("[");
        for (int i = 0; i < stack.size(); i++) {
            if(i == stack.size()-1) {
                System.out.print(((PrioriterizedEntry)stack.get(i)).input);
            } else {
                System.out.print(((PrioriterizedEntry)stack.get(i)).input + ", ");
            }
        }
        System.out.print("];");
        System.out.println();
    }
}

