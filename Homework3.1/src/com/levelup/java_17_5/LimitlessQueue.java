package com.levelup.java_17_5;

public class LimitlessQueue<E>  {
    private LimitlessStack<E> stack;
    private LimitlessStack<E> tmpStack;
    //StackBase .ctor
    public LimitlessQueue()
    {
        stack = new LimitlessStack<>();
        tmpStack = new LimitlessStack<>();
    }

    //Add an element to the queue
    public void push (E data){
        if (stack.isEmpty()) {
            stack.push(data);
        }
        else {
            while (!stack.isEmpty()){
                tmpStack.push(stack.pop());
            }
            stack.push(data);
            while (!tmpStack.isEmpty()){
                stack.push(tmpStack.pop());
            }
        }
    }

    //Remove front element from the queue
    public E pop() {
        return (E) stack.pop();
    }

    public void print() {
        stack.print();
    }

}
