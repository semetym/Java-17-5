package com.levelup.java_17_5;

import java.util.Arrays;

public class CustomArrayList<E> {
    private static final int defaultSize = 5;
    private static final Object[] emptyArray = {};
    private int size;
    protected Object[] customArrayListElementData;

    // base .ctor
    public CustomArrayList() {
        super();
        this.customArrayListElementData = emptyArray;
    }

    public CustomArrayList(int initialCapacity) {
        super();
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal Capacity: " +
                    initialCapacity);
        this.customArrayListElementData = new Object[initialCapacity];
    }
    // size method
    public int size() {
        return size;
    }
    // isEmpty method
    public boolean isEmpty() {
        return size == 0;
    }

    // return true
    public boolean add(E e) {
        ensureCapacity(size + 1);
        customArrayListElementData[size++] = e;
        return true;
    }
    // add element
    public void add(int index, E element) {
        ensureCapacity(size + 1);
        System.arraycopy(customArrayListElementData, index, customArrayListElementData, index + 1, size - index);
        customArrayListElementData[index] = element;
        size++;
    }
    // delete all data
    public void clear() {
        for (int i = 0; i < size; i++)
            customArrayListElementData[i] = null;
        size = 0;
    }
    // get method
    @SuppressWarnings("unchecked")
    public E get(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("array index out of bound exception with index at" + index);
        }
        return (E) customArrayListElementData[index];
    }
    // set method

    public void set(int index, E element) {
        if (index < 0 && index >=size) {
            throw new ArrayIndexOutOfBoundsException("array index out of bound exception with index at" + index);
        }
        this.customArrayListElementData[index] = element;
    }
    @SuppressWarnings("unchecked")
    public E remove(int index) {
        E oldValue = (E) customArrayListElementData[index];
        int removeNumber = size - index - 1;
        if (removeNumber > 0) {
            System.arraycopy(customArrayListElementData, index + 1, customArrayListElementData, index, removeNumber);
        }
        customArrayListElementData[--size] = null;
        return oldValue;
    }

    private void growCustomArrayList(int minCapacity) {
        int oldCapacity = customArrayListElementData.length;
        int newCapacity = oldCapacity + (oldCapacity / 2);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        customArrayListElementData = Arrays.copyOf(customArrayListElementData, newCapacity);
    }

    private void ensureCapacity(int minCapacity) {
        if (customArrayListElementData == emptyArray) {
            minCapacity = Math.max(defaultSize, minCapacity);
        }
        if (minCapacity - customArrayListElementData.length > 0)
            growCustomArrayList(minCapacity);
    }
    public synchronized String toString() {
        return super.toString();
    }
}
